# Drone Recognition project

Deep Learning project.

#Tester la reconnaissance du drone

Se rendre au niveau du dossier "keras-yolo3" et lancer une des commandes suivantes :

```
python yolo_video.py --input drone_test.mp4
python yolo_video.py --input drone_test_2.mp4
python yolo_video.py --input video_test.m4v
python yolo_video.py --input ../datasets/youtube/benoit/Park_Kenya.mp4
python yolo_video.py --input ../datasets/youtube/benoit/1.mp4
python yolo_video.py --input ../datasets/youtube/benoit/2.mp4

```